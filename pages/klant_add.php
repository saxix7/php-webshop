<?php
if(!isset($_SESSION["ID"])&&($_SESSION["STATUS"]!="ACTIEF")){
    echo "<script> alert('U heeft geen toegang tot deze pagina.');
    location.href='../index.php'; </script>";
}
include("klant_add.html");
if(isset($_POST["submit"])) {
    $melding = "";
    $voornaam = htmlspecialchars($_POST['voornaam']);
    $achternaam = htmlspecialchars($_POST['achternaam']);
    $straat = htmlspecialchars($_POST['straat']);
    $postcode = htmlspecialchars($_POST['postcode']);
    $woonplaats = htmlspecialchars($_POST['woonplaats']);
    $email = htmlspecialchars($_POST['email']);

    $sql = "INSERT INTO klant (ID, voornaam, achternaam, straat, postcode, woonplaats, email) values (?,?,?,?,?,?,?)";
    $stmt = $verbinding->prepare($sql);
    try {
        $stmt->execute(array(NULL,$voornaam,$achternaam,$straat,$postcode,$woonplaats,$email));
        $melding = "Nieuwe klant toegevoegd.";
    }
    catch(PDOException $e) {
        $melding="Kon geen nieuwe klant aanmaken.".$e->getMessage();
    }
    echo "<div id='melding'>",$melding."</div>";
}
?>