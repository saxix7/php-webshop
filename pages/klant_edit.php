<?php
if(!isset($_SESSION["ID"])&&($_SESSION["STATUS"]!="ACTIEF")){
    echo "<script> alert('U heeft geen toegang tot deze pagina.');
    location.href='../index.php'; </script>";
}
$sql = "SELECT ID, voornaam, achternaam, straat, postcode, woonplaats, email FROM klant WHERE ID = ?";
$stmt = $verbinding->prepare($sql);
$stmt->execute(array($_GET['id']));
$klanten = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach($klanten as $klant) {
?>
<div class="content">
    <form name="edit" class="form" action="index.php?page=klant_update" method="POST">
        <p id="page_titel">Edit klant</p>
        <input type="hidden" name="id" id="id" value="<?php echo $klant['ID']; ?>" />
        <label>Voornaam:</label>
        <input type="text" name="voornaam" id="voornaam" value="<?php echo $klant['voornaam']; ?>" />
        <label>Achternaam:</label>
        <input type="text" name="achternaam" id="achternaam" value="<?php echo $klant['achternaam']; ?>" />
        <label>Straat:</label>
        <input type="text" name="straat" id="straat" value="<?php echo $klant['straat']; ?>" />
        <label>Postcode:</label>
        <input type="text" name="postcode" id="postcode" value="<?php echo $klant['postcode']; ?>" />
        <label>Woonplaats:</label>
        <input type="text" name="woonplaats" id="woonplaats" value="<?php echo $klant['woonplaats']; ?>" />
        <label>Email:</label>
        <input type="text" name="email" id="email" value="<?php echo $klant['email']; ?>" />
        <br>
        <div class="icon_container">
            <input type="submit" class="icon" id="submit" name="submit" value="&rarr;" />
        </div>
        <a href="index.php?page=klanten">Terug</a>
    </form>
</div>
<?php
}
?>