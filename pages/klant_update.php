<?php
if(!isset($_SESSION["ID"])&&($_SESSION["STATUS"]!="ACTIEF")){
    echo "<script> alert('U heeft geen toegang tot deze pagina.');
    location.href='../index.php'; </script>";
}
if(isset($_POST['submit'])){
    $id = htmlspecialchars($_POST['id']);
    $voornaam = htmlspecialchars($_POST['voornaam']);
    $achternaam = htmlspecialchars($_POST['achternaam']);
    $straat = htmlspecialchars($_POST['straat']);
    $postcode = htmlspecialchars($_POST['postcode']);
    $woonplaats = htmlspecialchars($_POST['woonplaats']);
    $email = htmlspecialchars($_POST['email']);
    $sql = "UPDATE klant SET voornaam = ?, achternaam = ?, straat = ?, postcode = ?, woonplaats = ?, email = ? WHERE ID = ?";
    $stmt = $verbinding->prepare($sql);
    try {
        $stmt = $stmt->execute(array($voornaam, $achternaam, $straat, $postcode, $woonplaats, $email, $id));
        echo "<script>alert('Klant is geupdatet.');
        location.href='index.php?page=klanten'; </script>";
    }catch(PDOException $e) {
        echo $e->getMessage();
    }
}
?>